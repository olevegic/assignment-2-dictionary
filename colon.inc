%define elem 0
%macro colon 2
    %ifstr %1
        %ifid %2
            %2:
                dq elem
                db %1, 0
        %else
            %error "id value must be id"
        %endif
    %else   
        %error "string value must be string"
    %endif
    %define elem %2
%endmacro