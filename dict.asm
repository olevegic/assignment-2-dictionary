%include "lib.inc"
%define psize 8
section .text
global find_word

find_word:
    push r12
    push r13
    .loop:
        mov r12, rdi
        mov r13, rsi

        add rsi, psize
        call string_equals

        mov rsi, r13
        mov rdi, r12

        cmp rax, 1
        je .found

        mov rsi, [rsi]
        test rsi, rsi
        jz .not_found

        jmp .loop

    .found:
        mov rax, rsi
        pop r13
        pop r12
        ret

    .not_found:
        xor rax, rax
        pop r13
        pop r12
        ret

