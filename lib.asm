global exit
global string_length
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
global print_string


section .text

; defining to easy read and code
%define exit_call 60

%define newline '\n'

%define read_call 0
%define write_call 1

%define stdin 0
%define stdout 1
%define stderr 2

%define uint_max 20
%define ascii_d '0'


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, exit_call
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    
    .loop:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
   push rdi

   ; get string length
   call string_length
   mov rdx, rax
   mov rax, write_call ; write
   mov rdi, stdout ; stdout
   
   ; recover
   pop rsi
   syscall
   ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`

; Принимает код символа и выводит его в stdout
print_char:
    push di ; push char
    mov rax, write_call
    mov rsi, rsp ; char to rsi
    mov rdx, 1 ;length
    mov rdi, stdout
    syscall
    pop di ;recover
    ret
    
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint ; if pos -> jump
    
    ;neg to pos
    neg rdi 
    push rdi
    mov dil, '-'
    call print_char
    pop rdi

    
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    sub rsp, uint_max+1 ; null-term
    mov rax, rdi
    mov rsi, 10 ; dec
    mov rdi, rsp ; buffer start
    add rdi, uint_max ; end buffer
    mov byte [rdi], 0

    .loop:
        xor rdx, rdx
        div rsi 
        dec rdi ; move pointer 
        add dl, ascii_d
        mov [rdi], dl
        test rax, rax
        jnz .loop
    
    .end:
        call print_string
        add rsp, uint_max+1 ; reset
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .loop:
        mov al, [rdi+rcx] ; byte from 1
        cmp al, byte[rsi+rcx] ; compare bytes
        jne .not_equal 
        inc rcx ;inc pointer
        test al, al ; check last
        jnz .loop
    
    .equal:
        mov rax,1
        ret
    
    .not_equal:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push word 0

    mov rax, read_call
    mov rdi, stdin
    mov rsi, rsp ; pointer to buffer
    mov rdx, 1
    syscall

    test rax, rax ; check for end
    jz .end

    xor rax, rax 
    pop ax
    ret

    .end:
        pop ax
        ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12 ; save
    push r14

    mov r12, rdi ;buffer 

    test rsi, rsi
    jz .pure ; length == 0

    push r13
    mov r13, rsi ; push length

    .spaces:
        call read_char ; read and skip any incorrect
        cmp al, ' '
        je .spaces
        cmp al, `\n`
        je .spaces
        cmp al, `\t`
        je .spaces
        test rax, rax
        jz .success

    .loop:
        test rax, rax
        jz .success

        cmp r13, 1
        jbe .err  ; no null-term
        mov byte[r12+r14], al
        dec r13                     
        inc r14                     
        call read_char

        cmp al, ' ' ; end
        je .success
        cmp al, `\n`
        je .success
        cmp al, `\t`
        je .success
        jmp .loop

    .success:
        mov byte[r12+r14], 0 ; null-term

    .pure:
        mov rax, r12
        mov rdx, r14
        jmp .end

    .err:
        mov rax, 0
    
    .end:
        pop r13
        pop r14
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rax, rax
    xor rdx, rdx

    mov rsi, 10 ; dec

    .loop:
        mov cl, byte [rdi+rdx] ; current char

        sub cl, '0'
        test cl, cl ; null-term
        jl .end

        cmp cl, 9 
        jg .end

        inc rdx
        imul rax, rsi
        add rax, rcx ; add digit
        jmp .loop

    .end:
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-' ; check minus
    je .neg

    cmp byte [rdi], '+' ; check plus
    je .plus

    call parse_uint
    ret

    .plus:
        inc rdi ; skip +
        call parse_uint
        inc rdx ; inc length
        ret

    .neg:
        inc rdi ; skip -
        call parse_uint
        neg rax ; neg res
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx

    .loop:
        cmp rcx, rdx ; compare
        jge .end ; i >= buff.length

        mov al, byte [rdi+rcx] ; load
        mov byte [rsi+rcx], al  ; store

        inc rcx

        test al, al ; null-term
        je .terminate

        jmp .loop
    
    .terminate:
        mov rax, rcx ; length
        ret

    .end:
        xor rax, rax
        ret

print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, write_call
    mov rdi,  stderr
    syscall
    ret