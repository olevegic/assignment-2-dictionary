%include "words.inc"
%include "lib.inc"

%define buff_size 255
%define psize 8

section .rodata
not_found: db "key not found", 0
key_too_long: db "key is too long (>255)", 0

section .bss
buffer resb 256



section .text

global _start

extern find_word

_start:
    mov rdi, buffer
    mov rsi, buff_size
    call read_word

    test rax, rax
    jz .key_too_long

    mov rdi, rax
    mov rsi, elem
    call find_word

    test rax, rax
    jz .key_not_found

    mov rdi, rax
    add rdi, psize
    push rdi
    call string_length

    pop rdi
    lea rdi, [rdi+rax+1]
    call print_string

    xor rdi, rdi
    call exit

    .key_too_long:
        mov rdi, key_too_long
        call print_error
        call exit
    
    .key_not_found:
        mov rdi, not_found
        call print_error
        call exit



