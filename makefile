ASM = nasm 
ASMFLAG = -f elf64
LD = ld
PY = python3

.PHONY: all


%.o: %.asm
	${ASM} ${ASMFLAG} -o $@ $<

program: lib.o dict.o main.o
	$(LD) -o $@ $^

test:
	$(PY) test.py

clean:
	rm -f *.o program

all:	sum

sum:	clean program test

main.o:	main.asm words.inc colon.inc lib.inc dict.inc
dict.o:	dict.asm lib.inc
words.inc:	colon.inc

