import subprocess


test_cases = [
    {
        'input': 'first_w',
        'expected_output': 'first_word_explanation',
        'expected_error': '',
    },
    {
        'input': 'second_w',
        'expected_output': 'second_word_explanation',
        'expected_error': '',
    },
    {
        'input': 'third_w',
        'expected_output': 'third_word_explanation',
        'expected_error': '',
    },
    {
        'input': 'please',
        'expected_output': '',
        'expected_error': 'key not found',
    },
    {
        'input': "don't",
        'expected_output': '',
        'expected_error': 'key not found',
    },
    {
        'input': 'judge',
        'expected_output': '',
        'expected_error': 'key not found',
    },
    {
        'input': 'strictly',
        'expected_output': '',
        'expected_error': 'key not found',
    },
    {
        'input': 'help' * 65,
        'expected_output': '',
        'expected_error': 'key is too long (>255)',
    },
    {
        'input': 'me' * 130,
        'expected_output': '',
        'expected_error': 'key is too long (>255)',
    },
]

wrong = 0

for test_case in test_cases:
    process = subprocess.run("./program", input=test_case['input'], text=True, capture_output=True)

    if (
        process.stdout != test_case['expected_output']
        or process.stderr != test_case['expected_error']
    ):
        print("Test failed")
        print("Input:", test_case['input'])
        print("Expected Output:", test_case['expected_output'])
        print("Expected Error:", test_case['expected_error'])
        print("Actual Output:", process.stdout)
        print("Actual Error:", process.stderr)

        wrong += 1

print("Number of wrong answers =", wrong)

